package com.company;

/**
 * Created by Pter on 09.05.2016.
 */
public class JMXAttribute {
    public String domainName;
    public String objectName;
    public String attributeName;
    public String keyName;
    public String className;

    public JMXAttribute(String domainName, String objectName, String attributeName, String className, String key) {
        this.domainName = domainName;
        this.objectName = objectName;
        this.attributeName = attributeName;
        this.keyName = key;
        this.className = className;
    }
    public JMXAttribute(String domainName, String objectName, String attributeName, String className) {
        this.domainName = domainName;
        this.objectName = objectName;
        this.attributeName = attributeName;
        this.keyName = null;
        this.className = className;
    }
}
