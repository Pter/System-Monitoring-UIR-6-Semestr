package com.company;

import javax.management.JMX;
import java.util.TimerTask;

/**
 * Created by Pter on 09.05.2016.
 */
public class JMXMonitoringTask extends TimerTask {
    private JMXAttribute attribute;
    private JMXAttributeMonitor monitor;

    public JMXMonitoringTask(JMXAttribute attribute,JMXAttributeMonitor monitor){
        this.attribute = attribute;
        this.monitor = monitor;
    }

    @Override
    public void run(){
        monitor.monitorAttribute(attribute);
    }
}
