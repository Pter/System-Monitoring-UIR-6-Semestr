package com.company;

import javax.management.*;
import java.lang.reflect.*;
import java.lang.annotation.Annotation;
import java.io.Serializable;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by Pter on 04.05.2016.
 */
public class JMXHandler {

    private JMXConnector connector = null;
    private String hostname = null;
    private Integer port = null;
    private boolean connected = false;

    private String username = null;
    private String password = null;


    //constructor receives hostname and port of jmx-agent
    public JMXHandler(String hostname, Integer port){
        this.hostname = hostname;
        this.port = port;
    }
    //constructor for username-password authentication
    public JMXHandler(String hostname, Integer port, String username, String password){
        this.hostname = hostname;
        this.port = port;
        this.username = username;
        this.password = password;
    }

    public boolean isConnected(){
        return this.connected;
    }

    //return true if connected, false otherwise
    public boolean connect()  {
        //create parameters map
        HashMap options = new HashMap();
        //authentication
        if (this.username != null){
            String[] credentials = new String[2];
            credentials[0] = username;
            credentials[1] = password;
            options.put("jmx.remote.credentials", credentials);
        }


        //create JMXConnector instance for our connection
        try {
            this.connector = JMXConnectorFactory.newJMXConnector(createConnectionURL(this.hostname, this.port), options);
        } catch (IOException e) {
            e.printStackTrace();
            this.connected = false;
            return false;
        }
        // if connector successfully created, then try to connect server
        try {
            this.connector.connect();
        } catch (IOException e) {
            e.printStackTrace();
            this.connected = false;
            return false;
        }
        this.connected = true;
        return true;
        //TODO - add better logging for errors

    }

    public Object getAttribute(String domainName, String objectName, String attributeName, String className) throws Exception {
        return this.getAttribute( domainName,  objectName, attributeName, className, null);
    }

    //TODO - improve exception catching
    public Object getAttribute(String domainName, String objectName, String attributeName, String className, String keyName) throws Exception {
        Object obj = null;
        ObjectName objName = new ObjectName(domainName + ":type=" + objectName);

        obj = this.connector.getMBeanServerConnection().getAttribute(objName, attributeName);
//        System.out.println(obj.getClass());
//        System.out.println(obj);
//        System.out.println(obj.getClass().toString());
        if (obj.getClass().toString().equals("class " + className) ){
            return obj;
        } else {
            //try to receive needed class
            if (keyName != null && obj.getClass().toString().equals("class javax.management.openmbean.CompositeDataSupport")) {
                CompositeData cData = (CompositeData) obj;

               // System.out.println(cData.getCompositeType().getTypeName());
//                Class clazz = obj.getClass();
//
//                // show class fields
//                Field[] fields = clazz.getDeclaredFields();
//                for (Field field : fields) {
//                    System.out.println("\t" + getModifiers(field.getModifiers())
//                            + getType(field.getType()) + " " + field.getName() + ";");
//                }
//
//                // show class methods
//                Method[] methods = clazz.getDeclaredMethods();
//                for (Method m : methods) {
//                    // получаем аннотации
//                    Annotation[] annotations = m.getAnnotations();
//                    System.out.print("\t");
//                    for (Annotation a : annotations)
//                        System.out.print("@" + a.annotationType().getSimpleName() + " ");
//                    System.out.println();
//
//                    System.out.print("\t" + getModifiers(m.getModifiers()) +
//                            getType(m.getReturnType()) + " " + m.getName() + "(");
//                    System.out.print(getParameters(m.getParameterTypes()));
//                    System.out.println(") { }");
//                }

                Object ans = cData.get(keyName);
                //System.out.println(ans);
                //System.out.println(ans.getClass());
                if (ans.getClass().toString().equals("class " + className) ) {
                    return ans;
                } else {
                    throw new Exception("Cannot find requested attribute "+attributeName+" .");
                }
            }

            throw new Exception("Cannot find requested attribute "+attributeName+" .");
        }

    }

    static String getModifiers(int m) {
        String modifiers = "";
        if (Modifier.isPublic(m)) modifiers += "public ";
        if (Modifier.isProtected(m)) modifiers += "protected ";
        if (Modifier.isPrivate(m)) modifiers += "private ";
        if (Modifier.isStatic(m)) modifiers += "static ";
        if (Modifier.isAbstract(m)) modifiers += "abstract ";
        return modifiers;
    }

    static String getType(Class clazz) {
        String type = clazz.isArray()
                ? clazz.getComponentType().getSimpleName()
                : clazz.getSimpleName();
        if (clazz.isArray()) type += "[]";
        return type;
    }

    static String getParameters(Class[] params) {
        String p = "";
        for (int i = 0, size = params.length; i < size; i++) {
            if (i > 0) p += ", ";
            p += getType(params[i]) + " param" + i;
        }
        return p;
    }

    //this function used to create JMXServiceURL, that used in connection to jmx-agent
    private static JMXServiceURL createConnectionURL(String host, int port) throws MalformedURLException {
        return new JMXServiceURL("rmi", "", 0, "/jndi/rmi://" + host + ":" + port + "/jmxrmi");
    }

}
