package com.company;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.*;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Pter on 30.05.2016.
 */
public class JMXWebInterface {

    JMXAttributeMonitor monitor = null;
    int port;

    public JMXWebInterface(JMXAttributeMonitor monitor, int port){
        this.monitor = monitor;
        this.port = port;
    }

    // Starts web interface on defined port. True if success, false otherwise
    public boolean start(){
        HttpServer server = null;
        try {
            server = HttpServer.create();
            server.bind(new InetSocketAddress(this.port), 0);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        server.createContext("/", new MainPageHandler());
        server.createContext("/api/getParamGraph", new getParamGraphHandler(this));
        server.start();

        return true;
    }

    static class getParamGraphHandler implements HttpHandler {

        JMXWebInterface web = null;

        public getParamGraphHandler(JMXWebInterface web){
            this.web = web;
        }

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            try {
                //System.out.println(exchange.getRequestURI().toString());
                String path = exchange.getRequestURI().toString();
                JMXAttribute attr = this.web.monitor.getAttributesArray()[1];
                System.out.println(attr);
                String str = this.web.monitor.getAttributeHistoryJSON(attr);
                System.out.println(str);
                int resposeCode = 404;


                byte[] bytes = str.getBytes();
                exchange.sendResponseHeaders(200, bytes.length);

                OutputStream os = exchange.getResponseBody();
                os.write(bytes);
                os.close();
            } catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }



    static class MainPageHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            //System.out.println(exchange.getRequestURI().toString());
            String path = exchange.getRequestURI().toString();
            String str = "Not Found";
            int resposeCode = 404;
            if (path.equals("/")) {
                byte[] encoded = Files.readAllBytes(Paths.get("web/index.html"));
                str = new String(encoded);
            }
            if (path.startsWith("/css/") || path.startsWith("/js/") || path.startsWith("/bootstrap/")){
                try {
                    byte[] encoded = Files.readAllBytes(Paths.get("web" + path));
                    str = new String(encoded);
                } catch (IOException ex){

                }
            }


            byte[] bytes = str.getBytes();
            exchange.sendResponseHeaders(200, bytes.length);

            OutputStream os = exchange.getResponseBody();
            os.write(bytes);
            os.close();
        }
    }



}
