package com.company;

import javax.management.JMX;
import java.lang.reflect.Array;
import java.sql.*;
import java.util.ArrayList;
import java.util.Timer;

/**
 * Created by Pter on 09.05.2016.
 */
public class JMXAttributeMonitor {

    private ArrayList<JMXAttribute> monitoredAttributes;
    private JMXHandler jmx;
    private Timer timer = new Timer();
    private JMXWebInterface web = new JMXWebInterface(this, 9090);

    private Connection db;


    public JMXAttributeMonitor(String hostname, Integer port) {
        monitoredAttributes = new ArrayList();
        jmx = new JMXHandler(hostname,port);

        //
        this.db = null;
        try {
            Class.forName("org.sqlite.JDBC");
            this.db = DriverManager.getConnection("jdbc:sqlite:database.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Opened database successfully");

    }

    public boolean connect(){
        boolean started = jmx.connect();
        if (started){
            this.web.start();
        }
        return started;
    }

    //returns true if added, false otherwise
    public boolean addAttribute(JMXAttribute attribute, int interval){
        //before adding attribute to monitoring, check that we can receive it correctly.
        try {
            jmx.getAttribute(attribute.domainName, attribute.objectName, attribute.attributeName, attribute.className, attribute.keyName);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        this.monitoredAttributes.add(attribute);
        //creating table if not exist
        Statement st;
        try {

            st = this.db.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        try {
            st.execute(generateCreateTableQuery(attribute));
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        JMXMonitoringTask monTask = new JMXMonitoringTask(attribute,this);
        timer.scheduleAtFixedRate(monTask, 0, interval);
        return true;
    }




    public Object monitorAttribute(JMXAttribute attribute){
        Object res = null;
        try {
            res = jmx.getAttribute(attribute.domainName, attribute.objectName, attribute.attributeName, attribute.className, attribute.keyName);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        System.out.println(res);
        //insert value in table

        Statement st = null;
        try {
            st = this.db.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            st.execute("INSERT INTO \"" + generateTableName(attribute) + "\" VALUES ("+System.currentTimeMillis()+","+res.toString()+")");
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return null;
    }

    private String generateCreateTableQuery(JMXAttribute attribute){
        String type = attribute.className.replace("java.lang.","");
        String name = generateTableName(attribute);


        String query = "CREATE TABLE IF NOT EXISTS \"" + name + "\"  (\n" +
                "    timestamp INTEGER,\n" +
                "    value     " + type + "\n" +
                ");\n";
        System.out.println("Generated query: " + query);
        return query;
    }

    private String generateTableName(JMXAttribute attribute){
        String name = attribute.domainName + "." +  attribute.objectName + "_" + attribute.attributeName;
        if (attribute.keyName != null){
            name = name + "_" + attribute.keyName;
        }
        return name;
    }

    public String getAttributeHistoryJSON(JMXAttribute attribute){
        Statement st = null;
        try {
            st = this.db.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ResultSet res = null;
        String query = "Select * FROM '" + generateTableName(attribute) + "' ORDER BY timestamp DESC LIMIT 500";
        System.out.println(query);
        try {
            res = st.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String json = "[";
        try {
            while(res.next()){
                try {
                    json = json + "[" +res.getObject("timestamp").toString() + "," + res.getObject("value").toString() + "],";
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        json = json.substring(0,json.length()-1);
        json = json + "]";
        return json;
    }

    public JMXAttribute[] getAttributesArray(){
        return (JMXAttribute[]) this.monitoredAttributes.toArray(new JMXAttribute[this.monitoredAttributes.size()]);
    };

    private int getCurrentTimestamp(){
        return (int) Math.round(System.currentTimeMillis()/1000);
    }
}
