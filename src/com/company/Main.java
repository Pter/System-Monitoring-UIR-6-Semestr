package com.company;

import javax.management.*;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.lang.management.MemoryUsage;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Timer;
import java.util.Set;
import java.util.TimerTask;

/**
 * Created by Pter on 04.05.2016.
 */
public class Main {
    public static void main(String[] args) throws Exception {
//        Timer t = new Timer();
//        TimerTask task = new TimerTask() {
//            @Override
//            public void run() {
//                System.out.println("Task executed");
//            }
//        };
//
//        t.scheduleAtFixedRate(task, 0, 1000 );
        JMXAttributeMonitor mon = new JMXAttributeMonitor("127.0.0.1",9010);
        mon.connect();
        JMXAttribute atr =  new JMXAttribute("java.lang","ClassLoading","LoadedClassCount","java.lang.Integer");
        mon.addAttribute(atr,2000);

        JMXAttribute atrInitMemory =  new JMXAttribute("java.lang","Memory","HeapMemoryUsage","java.lang.Long","used");
        mon.addAttribute(atrInitMemory,2000);
        //mon.monitorAttribute(atr);




        //ConnectToJMX();
//        JMXHandler jmx = new JMXHandler("127.0.0.1",9010);
//        jmx.connect();
//        Object ans = jmx.getAttribute("java.lang","ClassLoading","LoadedClassCount","java.lang.Integer");
//        System.out.println(ans);
//        System.out.println("_______________");
//        Object ans2 = jmx.getAttribute("java.lang", "Memory","HeapMemoryUsage","java.lang.Long","init");
//        System.out.println(ans2);
//        Object ans3 = jmx.getAttribute("java.lang:type=GarbageCollector,name=PS MarkSweep","LastGcInfo");
//        System.out.println(ans3);



    }


    public static void ConnectToJMX2() throws Exception {
//        JmxClient client = new JmxClient("127.0.0.1", 9010);
//// get the set of bean names exported by the JVM
//        Set<ObjectName> objectNameSet = client.getBeanNames();
//// get the start-time in milliseconds
//        long startTimeMillis = (Long)client.getAttribute(new ObjectName("java.lang:type=Runtime"), "StartTime");
//// run garbage collection
//        client.invokeOperation(new ObjectName("java.lang:type=Memory"), "gc");
//        System.out.println("Starttime: "+startTimeMillis);
    }

    public static void ConnectToJMX() throws IOException, MalformedObjectNameException, AttributeNotFoundException, MBeanException, ReflectionException, InstanceNotFoundException {

        String host ="127.0.0.1";
        int port = 9010;
        HashMap map = new HashMap();
        //String[] credentials = new String[2];
        //credentials[0] = user;
        //credentials[1] = password;
        //map.put("jmx.remote.credentials", credentials);
        JMXConnector c = JMXConnectorFactory.newJMXConnector(createConnectionURL(host, port), map);
        c.connect();
        Object o = c.getMBeanServerConnection().getAttribute(new ObjectName("java.lang:type=Memory"), "HeapMemoryUsage");
        CompositeData cd = (CompositeData) o;
        System.out.println(cd.get("committed"));
        System.out.println(cd);

    }

    private static JMXServiceURL createConnectionURL(String host, int port) throws  MalformedURLException {
        return new JMXServiceURL("rmi", "", 0, "/jndi/rmi://" + host + ":" + port + "/jmxrmi");
    }
}
